package com.example.abc.abc;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AbcApplicationTests {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    public void contextLoads() {
        System.out.println("res: " + passwordEncoder.encode("root"));

        Assert.assertTrue(passwordEncoder != null);
    }
}
