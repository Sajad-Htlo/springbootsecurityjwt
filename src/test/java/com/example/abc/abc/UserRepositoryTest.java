package com.example.abc.abc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.containers.MySQLContainer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * Test Mysql with lunching a MySQL container.
 *
 * @author sajad
 */
public class UserRepositoryTest {

    @ClassRule
    public static MySQLContainer mySQLContainer = new MySQLContainer()
            .withDatabaseName("test2")
            .withPassword("root1")
            .withUsername("root1");

    @Test
    public void testMySQLContainerWithInitScriptFile() throws SQLException {
        performTestForScriptedSchema("jdbc:tc:mysql://hostname/test2?TC_INITSCRIPT=container_init_mysql.sql");
    }

    private void performTestForScriptedSchema(String jdbcUrl) throws SQLException {
        HikariDataSource dataSource = getDataSource(jdbcUrl);
        new QueryRunner(dataSource).query("SELECT name from authority where name like '%Test%'", new ResultSetHandler<Object>() {
            @Override
            public Object handle(ResultSet rs) throws SQLException {
                rs.next();
                String resultSetString = rs.getString(1);
                assertEquals("My Test AAA", resultSetString);
                return true;
            }
        });
        dataSource.close();
    }

    private HikariDataSource getDataSource(String jdbcUrl) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(jdbcUrl);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setMinimumIdle(1);
        hikariConfig.setMaximumPoolSize(1);

        return new HikariDataSource(hikariConfig);
    }
}
