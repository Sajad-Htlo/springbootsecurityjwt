package com.example.abc.abc;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.filter.IColumnFilter;
import org.springframework.test.context.TestContext;

import java.lang.reflect.Field;

public class MyDbUnitTestExecutionListener extends DbUnitTestExecutionListener {

    static class NullPrimaryKeyFilter implements IColumnFilter {
        private String[] keys = null;

        NullPrimaryKeyFilter(String... keys) {
            this.keys = keys;
        }

        public boolean accept(String tableName, Column column) {
            for (String key : keys) {
                if (column.getColumnName().equalsIgnoreCase(key)) {
                    return true;
                }
            }
            return false;
        }
    }

    @Override
    public void prepareTestInstance(TestContext testContext) throws Exception {
        super.prepareTestInstance(testContext);
        Object attribute = testContext.getAttribute(CONNECTION_ATTRIBUTE);
        Field connectionsFiled = attribute.getClass().getDeclaredField("connections");
        connectionsFiled.setAccessible(true);

        IDatabaseConnection[] a = (IDatabaseConnection[]) connectionsFiled.get(attribute);
        for (IDatabaseConnection c : a) {
            c.getConfig().setProperty(DatabaseConfig.PROPERTY_PRIMARY_KEY_FILTER,
                    new NullPrimaryKeyFilter("id", "user_id"));
        }
    }


}
