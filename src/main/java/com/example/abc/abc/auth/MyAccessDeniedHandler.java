package com.example.abc.abc.auth;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyAccessDeniedHandler implements AccessDeniedHandler {

    /**
     * User is authenticated but has no right to
     * access to the requested resource
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e)
            throws IOException {
        response.setStatus(403);
        response.setHeader("content-type", "application/json");
        response.getWriter().write("You do not have sufficient right to access this resource");
    }
}
