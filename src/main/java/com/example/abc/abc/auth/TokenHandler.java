package com.example.abc.abc.auth;

import com.example.abc.abc.domain.User;
import com.example.abc.abc.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenHandler {

    @Autowired
    private UserRepository userRepository;

    @Value("${token.expiration}")
    private long EXPIRATION_TIME = 0;

    @Value("${token.secret}")
    private String SECRET = "";

    String createTokenForUser(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    User parsUserFromToken(String token) {
        // parse the token to find username.
        String username = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        return userRepository.findByUsername(username);
    }
}
