package com.example.abc.abc.auth;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -8970718410437077606L;

    /**
     * This is called when a user tries to access a secured resource without providing any token.
     * We should just send 1 401 unauthorized response because there is no login page to redirect to.
     *
     * @param request       Http request.
     * @param response      Http response.
     * @param authException Authentication exception.
     * @throws IOException IO exception.
     */
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        response.setStatus(401);
        response.setHeader("content-type", "application/json");
        response.getWriter().write("You are not authorized (Not authenticated).");
    }
}