package com.example.abc.abc.auth;

import com.example.abc.abc.MyUtility;
import com.example.abc.abc.domain.User;
import com.example.abc.abc.dto.TokenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class TokenAuthenticationService {

    private final TokenHandler tokenHandler;

    private static final String TOKEN_HEADER = "Authorization";

    @Autowired
    public TokenAuthenticationService(TokenHandler tokenHandler) {
        this.tokenHandler = tokenHandler;
    }

    void addAuthentication(HttpServletResponse response, String username) throws IOException {
        // Create token via username and secret and set its expiration date
        String token = tokenHandler.createTokenForUser(username);

        // Add token DTO to response body
        response.setHeader("content-type", "application/json");
        response.getWriter().write(MyUtility.objectMapper().writeValueAsString(new TokenDTO("Welcome", token)));
        response.getWriter().flush();
        response.getWriter().close();
    }

    Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(TOKEN_HEADER);

        if (token != null) {
            // parse the token to find username.
            User user = tokenHandler.parsUserFromToken(token);

            return user != null ?
                    new UsernamePasswordAuthenticationToken(user.getUsername(), null, user.getAuthorities()) :
                    null;
        }
        return null;
    }
}