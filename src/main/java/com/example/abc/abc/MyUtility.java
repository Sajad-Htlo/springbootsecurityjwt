package com.example.abc.abc;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MyUtility {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static ObjectMapper objectMapper(){
        return objectMapper;
    }
}
