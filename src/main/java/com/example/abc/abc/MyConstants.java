package com.example.abc.abc;

public final class MyConstants {

    public static class URLMapping {
        static final String API_PREFIX = "/api";

        public static final String BOOKS = API_PREFIX + "/books";

        public static final String USERS = API_PREFIX + "/users";
    }

    /**
     * No-Op private constructor enforcing final status
     */
    private MyConstants() {
        throw new IllegalAccessError("Cannot be instantiated.");
    }

}
