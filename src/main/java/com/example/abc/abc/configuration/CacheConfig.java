package com.example.abc.abc.configuration;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Guava cache config.
 *
 * @author sajad
 */
@Configuration
public class CacheConfig {
    public static final String USER_CACHE = "USER";

    /**
     * Define cache manager.
     *
     * @return object of guava cache manager.
     */
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        GuavaCacheManager guavaCacheManager = new GuavaCacheManager();

        GuavaCache userCache = new GuavaCache(USER_CACHE,
                CacheBuilder.newBuilder().maximumSize(2000).expireAfterWrite(15, TimeUnit.MINUTES).build());
        cacheManager.setCaches(Arrays.asList(userCache));

        return guavaCacheManager;
    }
}
