package com.example.abc.abc.controller;

import com.example.abc.abc.MyConstants;
import com.example.abc.abc.domain.Book;
import com.example.abc.abc.dto.BookListDTO;
import com.example.abc.abc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(MyConstants.URLMapping.BOOKS)
public class BookController {

    private final BookService bookService;
    private final ConversionService conversionService;

    @Autowired
    public BookController(BookService bookService,
                          ConversionService conversionService) {
        this.bookService = bookService;
        this.conversionService = conversionService;
    }

    @GetMapping
    public ResponseEntity getBooks(@PageableDefault(sort = "id",
            direction = Sort.Direction.ASC) Pageable pageable) {
        Page<Book> books = bookService.getBooks(pageable);

        return ResponseEntity.ok(books.map(
                book -> conversionService.convert(book, BookListDTO.class)));
    }
}
