package com.example.abc.abc.controller;

import com.example.abc.abc.MyConstants;
import com.example.abc.abc.domain.User;
import com.example.abc.abc.dto.UserDetailDto;
import com.example.abc.abc.dto.UserListDTO;
import com.example.abc.abc.dto.UserNewDTO;
import com.example.abc.abc.repository.UserRepository;
import com.example.abc.abc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.example.abc.abc.configuration.CacheConfig.USER_CACHE;

@RestController
@RequestMapping(MyConstants.URLMapping.USERS)
public class UserController {

    private final UserRepository userRepository;

    private final UserService userService;

    private final ConversionService conversionService;

    @Autowired
    public UserController(UserRepository userRepository,
                          UserService userService,
                          ConversionService conversionService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity getUsers(@PageableDefault(sort = "id",
            direction = Sort.Direction.ASC) Pageable pageable) {

        Page<User> users = userRepository.findAll(pageable);

        return ResponseEntity.ok(users
                .map(user -> conversionService.convert(user, UserListDTO.class)));
    }

    @GetMapping("/{id}")
    @Cacheable(value = USER_CACHE, key = "#id")
    public ResponseEntity getUserDetail(@PathVariable("id") long id) {
        User user = userService.getById(id);

        return ResponseEntity.ok(
                conversionService.convert(user, UserDetailDto.class));
    }

    @PostMapping()
    public ResponseEntity newUser(@RequestBody UserNewDTO newDTO) {
        User user = conversionService.convert(newDTO, User.class);

        long persistedId = userService.persistUser(user);

        return ResponseEntity.ok("Succeed, id: "+persistedId);
    }
}