package com.example.abc.abc.converter;

import com.example.abc.abc.domain.User;
import com.example.abc.abc.dto.UserNewDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserNewDTOToUserConverter implements Converter<UserNewDTO,User> {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User convert(UserNewDTO newDTO) {
        User user = new User();

        user.setUsername(newDTO.getUsername());
        user.setPassword(passwordEncoder.encode(newDTO.getPassword()));
        user.setEnabled(true);

        return user;
    }
}
