package com.example.abc.abc.converter;

import com.example.abc.abc.domain.User;
import com.example.abc.abc.dto.UserDetailDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDetailDtoConverter implements Converter<User, UserDetailDto> {

    @Override
    public UserDetailDto convert(User user) {
        UserDetailDto detailDto = new UserDetailDto();

        detailDto.setId(user.getId());
        detailDto.setUsername(user.getUsername());

        return detailDto;
    }
}
