package com.example.abc.abc.converter;

import com.example.abc.abc.domain.Book;
import com.example.abc.abc.dto.BookListDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BookToBookListDTOConverter implements Converter<Book, BookListDTO> {

    @Override
    public BookListDTO convert(Book book) {
        BookListDTO listDTO = new BookListDTO();

        listDTO.setId(book.getId());
        listDTO.setTitle(book.getTitle());

        return listDTO;
    }
}
