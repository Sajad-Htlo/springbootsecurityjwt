package com.example.abc.abc.converter;

import com.example.abc.abc.domain.User;
import com.example.abc.abc.dto.UserListDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserListDTOConverter implements Converter<User, UserListDTO> {


    @Override
    public UserListDTO convert(User user) {
        UserListDTO listDTO = new UserListDTO();

        listDTO.setId(user.getId());
        listDTO.setUsername(user.getUsername());

        return listDTO;
    }
}
