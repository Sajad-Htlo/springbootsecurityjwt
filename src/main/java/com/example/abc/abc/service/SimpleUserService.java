package com.example.abc.abc.service;

import com.example.abc.abc.domain.User;
import com.example.abc.abc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleUserService implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public SimpleUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getById(long id) {
        return userRepository.findOne(id);
    }

    @Override
    public long persistUser(User user) {
        User persistedUser = userRepository.save(user);

        return persistedUser.getId();
    }
}
