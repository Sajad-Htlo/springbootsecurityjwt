package com.example.abc.abc.service;

import com.example.abc.abc.domain.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {

    Page<Book> getBooks(Pageable pageable);
}
