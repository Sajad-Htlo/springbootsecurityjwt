package com.example.abc.abc.service;

import com.example.abc.abc.domain.User;

public interface UserService {



    User getById(long id);

    long persistUser(User user);
}
