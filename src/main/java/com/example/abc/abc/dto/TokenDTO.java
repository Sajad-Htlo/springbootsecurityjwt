package com.example.abc.abc.dto;

public class TokenDTO {

    private String message;
    private String token;

    public TokenDTO(String message, String token) {
        this.token = token;
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
