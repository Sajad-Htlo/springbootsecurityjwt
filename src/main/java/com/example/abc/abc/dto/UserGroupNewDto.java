package com.example.abc.abc.dto;

import java.util.HashSet;
import java.util.Set;

public class UserGroupNewDto {

    /**
     * Group name
     */
    private String name;

    /**
     * Group description
     */
    private String description;

    /**
     * Collection of group user ids
     */
    private Set<Long> userIds = new HashSet<>();

    /**
     * @return {@code name}
     * @see #name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name {@code name}
     * @see #name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return {@code description}
     * @see #description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description {@code description}
     * @see #description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return {@code userIds}
     * @see #userIds
     */
    public Set<Long> getUserIds() {
        return this.userIds != null ? this.userIds : new HashSet<>();
    }

    /**
     * @param userIds {@code userIds}
     * @see #userIds
     */
    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

}