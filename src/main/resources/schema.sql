ALTER TABLE book_authors
  DROP FOREIGN KEY FK551i3sllw1wj7ex6nir16blsm;
ALTER TABLE book_authors
  DROP FOREIGN KEY FKs4xm7q8i3uxvaiswj1c35nnxw;
ALTER TABLE comment
  DROP FOREIGN KEY FKkko96rdq8d82wm91vh2jsfak7;
ALTER TABLE user_authority
  DROP FOREIGN KEY FKgvxjs381k6f48d5d2yi11uh89;
ALTER TABLE user_authority
  DROP FOREIGN KEY FKpqlsjpkybgos9w2svcri7j8xy;

drop table if exists author;
drop table if exists authority;
drop table if exists book;
drop table if exists book_authors;
drop table if exists comment;
drop table if exists user;
drop table if exists user_authority;

create table author (id bigint not null auto_increment, first_name varchar(255) not null, primary key (id));
create table authority (id bigint not null auto_increment, name varchar(255) not null, primary key (id));
create table book (id bigint not null auto_increment, title varchar(255) not null, primary key (id));
create table book_authors (book_id bigint not null, authors_id bigint not null, primary key (book_id, authors_id));
create table comment (id bigint not null auto_increment, text varchar(255), book_id bigint, primary key (id));
create table user (id bigint not null auto_increment, email varchar(50), enabled bit not null, password varchar(100) not null, username varchar(50) not null, primary key (id));
create table user_authority (user_id bigint not null, authority_id bigint not null);

alter table user add constraint UK_sb8bbouer5wak8vyiiy4pf2bx unique (username);
alter table book_authors add constraint FK551i3sllw1wj7ex6nir16blsm foreign key (authors_id) references author (id);
alter table book_authors add constraint FKs4xm7q8i3uxvaiswj1c35nnxw foreign key (book_id) references book (id);
alter table comment add constraint FKkko96rdq8d82wm91vh2jsfak7 foreign key (book_id) references book (id);
alter table user_authority add constraint FKgvxjs381k6f48d5d2yi11uh89 foreign key (authority_id) references authority (id);
alter table user_authority add constraint FKpqlsjpkybgos9w2svcri7j8xy foreign key (user_id) references user (id);