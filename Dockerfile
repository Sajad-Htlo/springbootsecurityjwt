FROM openjdk:8
EXPOSE 8080
ADD /target/abc-0.0.1-SNAPSHOT.jar abc.jar
ENTRYPOINT ["java","-jar","abc.jar"]